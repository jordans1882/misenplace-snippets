;;; misenplace-snippets.el --- Collection of yasnippet snippets

;; Copyright (C) 2020 Jordan Schupbach

;; Author: Jordan Schupbach <jordan.schupbach@gmail.com>
;; Keywords: snippets
;; Version: 0.0.1
;; Package-Requires: ((yasnippet "0.8.0"))
;; Keywords: convenience, snippets

;;; Commentary:

;; snippet package for misenplace

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'yasnippet)

(defconst misenplace-snippets-dir
  (expand-file-name
   "snippets"
   (file-name-directory
    ;; Copied from ‘f-this-file’ from f.el.
    (cond
     (load-in-progress load-file-name)
     ((and (boundp 'byte-compile-current-file) byte-compile-current-file)
      byte-compile-current-file)
     (:else (buffer-file-name))))))

;;;###autoload
(defun misenplace-snippets-initialize ()
  "Load the `misenplace-snippets' snippets directory."
  ;; NOTE: we add the symbol `yasnippet-snippets-dir' rather than its
  ;; value, so that yasnippet will automatically find the directory
  ;; after this package is updated (i.e., moves directory).
  (add-to-list 'yas-snippet-dirs 'misenplace-snippets-dir t)
  (yas-load-directory misenplace-snippets-dir t))

(defgroup yasnippet-snippets nil
  "Options for yasnippet setups.
This is useful for customizing options declared in
“.yas-setup.el” files.  For example, you could declare a
customizable variable used for a snippet expansion.
See Info node `(elisp)Customization Types'."
  :group 'yasnippet)

(defun yasnippet-snippets--fixed-indent ()
  "Set `yas-indent-line' to `fixed'."
  (set (make-local-variable 'yas-indent-line) 'fixed))

(defun misenplace-snippets--no-indent ()
  "Set `yas-indent-line' to nil."
  (set (make-local-variable 'yas-indent-line) nil))

;;;###autoload
(eval-after-load 'yasnippet
   '(misenplace-snippets-initialize))


(provide 'misenplace-snippets)

;;; misenplace-snippets.el ends here
